<?php declare (strict_types=1);
    namespace GlintFMW\Configuration;

    use GlintFMW\Dependencies\Injector;

    /**
     * Interface for all configuration providers to implement
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Configuration
     */
    interface Provider
    {
        /**
         * Initializes the configuration provider on runtime
         *
         * @param array<string, mixed> $configuration The runtime configuration
         * @param Injector $dependencyInjector The dependency injector to use for settings
         */
        function __construct (array $configuration, Injector $dependencyInjector);

        /**
         * Indicates what section this config provider is parsing
         *
         * @return string
         */
        static function provides (): string;

        /**
         * Performs checks on the configuration and produces the actual settings
         * to be used on runtime
         *
         * @param array<int|string, mixed> $input The configuration data available on the JSON
         *
         * @return mixed
         *
         * @throws \GlintFMW\Configuration\Exception\IntegrityException When the configuration is not valid
         */
        static function convert (array $input);

        /**
         * Generates custom code for the configuration (for example including a different file)
         *
         * @param array<int|string, mixed> $input The configuration data available on the JSON
         *
         * @return string
         */
        static function customCode (array $input): string;

        /**
         * Used for sorting the way the configuration is generated and loaded
         *
         * @return int
         */
        static function priority (): int;

        /**
         * Allows for performing custom steps before generating the configuration
         */
        static function preGenerationStep (): void;

        /**
         * Allows for performing custom steps after generating the configuration
         */
        static function postGenerationStep (): void;
    };