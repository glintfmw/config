<?php declare (strict_types=1);
    /**
     * Simple utility to parse the config file and generate the output file
     *
     * @author Almamu
     */

    namespace GlintFMW\Configuration;

    use GlintFMW\Configuration\Exception\IntegrityException;

    use GlintFMW\Configuration\Provider;
    use GlintFMW\Application;

    /**
     * Takes care of config interpretation, loading and output generation. Used from bin/config
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Configuration
     */
    class Utility extends Application
    {
        /** @var string The default file to load the configuration from */
        const DEFAULT_CONFIG_FILE = "config/app.json";
        /** @var string The file where to write the configuration to */
        const DEFAULT_OUTPUT_FILE = "dist/configuration.php";

        /**
         * Checks of the given $param in $config and stops the script execution if it doesnt exist
         *
         * @param string $param The param to search in $config
         * @param array<string, mixed> $config The config to search the param within
         *
         * @return mixed The param value
         * @throws IntegrityException If the parameter was not found
         */
        protected function mustExistParam (string $param, array $config)
        {
            if (array_key_exists ($param, $config) == false)
                throw new IntegrityException ("Cannot find the param '" . $param . "' in config");

            return $config [$param];
        }

        /**
         * Checks if the given $param in $config exists and returns it's value
         * If it does not exist, returns $default
         *
         * @param string $param The param to search in $config
         * @param array<string, mixed> $config The config to search the param within
         * @param mixed $default Default value if parameter not found
         *
         * @return mixed The param value or the default value if not found
         */
        private function couldExistParam (string $param, array $config, $default = null)
        {
            if (array_key_exists ($param, $config) == false)
                return $default;

            return $config [$param];
        }

        /**
         * Runs the utility code
         *
         * @throws IntegrityException If the configuration is not correct
         * @throws \Exception If the directory cannot be created
         */
        function run (): void
        {
            // the command-line programs that we do care about
            $params = getopt ("f:", ["file:"]);

            // the config file to parse
            $file = self::DEFAULT_CONFIG_FILE;

            if (array_key_exists ('f', $params) == true && is_string ($params ['f']) === true)
                $file = $params ['f'];
            elseif (array_key_exists ('file', $params) == true && is_string ($params ['file']) === true)
                $file = $params ['file'];

            if (file_exists ($file) == false)
                throw new IntegrityException ("Cannot find the specified configuration file '{$file}'");

            $configFileContent = file_get_contents ($file);

            if ($configFileContent === false)
                throw new IntegrityException ("Cannot open file {$configFileContent} for reading");

            // the parsed config
            $config = json_decode ($configFileContent, true);

            // get the list of providers
            $providerList = $this->couldExistParam ('providers', $config, array());

            if (is_array ($providerList) == false)
                throw new IntegrityException ('The providers must be a list');

            // sort the array by priority
            $providerListSorted = array ();

            foreach ($providerList as $providerClass)
            {
                /** @var Provider $providerClass */
                $priority = $providerClass::priority ();

                if (array_key_exists ($priority, $providerListSorted) === false)
                    $providerListSorted [$priority] = array ();

                $providerListSorted [$priority] [] = $providerClass;
            }

            // sort the array by the key
            ksort ($providerListSorted);

            $configurationMap = array ();
            $customCodeMap = array ();

            foreach ($providerListSorted as $priority => $priorityClassList)
            {
                $configurationMap [$priority] = array ();
                $customCodeMap [$priority] = array ();

                foreach ($priorityClassList as $providerClass)
                {
                    /** @var class-string $providerClass */
                    $providerClass::preGenerationStep ();
                    $provides = $providerClass::provides ();
                    $data = $this->mustExistParam ($provides, $config);

                    $configurationMap [$priority] [$providerClass] = $providerClass::convert ($data);
                    $customCodeMap [$priority] [$providerClass] = $providerClass::customCode ($data);

                    $providerClass::postGenerationStep ();
                }
            }

            // finally generate the configuration file

            $content = "<?php declare (strict_types=1);" . PHP_EOL;

            // first load the custom code
            foreach ($customCodeMap as $priority => $codeList)
            {
                $content .= "\t// priority " . $priority . PHP_EOL;

                foreach ($codeList as $className => $code)
                {
                    $content .= "\t// custom code for " . $className . PHP_EOL;
                    $content .= "\t" . $code . PHP_EOL;
                }

                $content .= PHP_EOL . PHP_EOL;
            }

            // then add the return for the class configurations
            $content .= "\t// configuration map" . PHP_EOL;
            $content .= "\treturn " . var_export ($configurationMap, true) . ";";

            // ensure that the output folder exists first
            $directory = pathinfo (self::DEFAULT_OUTPUT_FILE, PATHINFO_DIRNAME);

            if (file_exists ($directory) == false)
                mkdir ($directory, 0700, true);
            elseif (is_dir ($directory) == false)
                throw new \Exception ("Cannot create output directory ({$directory}) for configuration...");

            // finally write the output
            file_put_contents (self::DEFAULT_OUTPUT_FILE, $content);
        }
    };