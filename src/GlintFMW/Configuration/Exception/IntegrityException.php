<?php declare(strict_types=1);
	namespace GlintFMW\Configuration\Exception;

    /**
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Configuration\Exception
     */
	class IntegrityException extends \Exception {};