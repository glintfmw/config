<?php declare (strict_types=1);
    namespace GlintFMW\Configuration\Providers;

    use GlintFMW\Dependencies\Injector;

    /**
     * Base configuration class for simple array-based configuration storage
     *
     * @author Alexis Maiquez Murcia <almamu@almamu.com>
     * @package GlintFMW\Configuration\Providers
     */
    abstract class Map implements \GlintFMW\Configuration\Provider
    {
        /** @var array<string, mixed> The configuration parameters for this instance */
        private array $configurationMap;
        /** @var Injector The dependency injector that this configurator belongs to */
        protected Injector $dependencyInjector;

        /** @inheritDoc */
        public function __construct (array $configuration, Injector $dependencyInjector)
        {
            $this->configurationMap = $configuration;
            $this->dependencyInjector = $dependencyInjector;
        }

        /**
         * @return array<string, mixed> The current configuration map
         */
        public function getConfiguration (): array
        {
            return $this->configurationMap;
        }

        /**
         * Indicates what section this config provider is parsing
         *
         * @return string
         */
        abstract static function provides (): string;

        static function convert (array $input)
        {
            return $input;
        }

        static function customCode (array $input): string
        {
            return "";
        }

        /**
         * Used for sorting the way the configuration is generated and loaded
         *
         * @return int
         */
        static function priority (): int
        {
            return 0;
        }

        static function preGenerationStep (): void
        {
        }

        static function postGenerationStep (): void
        {
        }
    };